/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Servlet;

import Persona.Entitys.Models;
import Persona.Entitys.PersonaLogin;
import Persona.Entitys.TabPersons;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Rubio
 */
@WebServlet(name = "Servlet_Inegrator", urlPatterns = {"/Servlet_Inegrator"})
public class Servlet_Inegrator extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String nombre=request.getParameter("nombre");
            String contrasena=request.getParameter("contrasena");
            Models model= new Models();
            PersonaLogin obj= new PersonaLogin();
            int idUserName=0;
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Servlet_Inegrator</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Tu nombre es :  " + request.getParameter("nombre") + "</h1>");
            out.println("<h1>Tu contraseña es:  " + request.getParameter("contrasena") + "</h1>");                
            idUserName=model.consultLastId();
            obj.setIdPersona(idUserName);
            obj.setNombre(nombre);
            obj.setPassword(contrasena);            
            
            
            try {
                model.salvar2(obj);
                out.printf("<h2>Registro insertado</h2>");
                /*
                String objres= request.getParameter("bottom");
                
                if(objres.equals("Registrarse")){                    
                   model.salvar2(obj);
                    out.printf("<h2>Registro insertado</h2>");
                }else{
                    if(model.consultarPorUserName(nombre)){
                        out.printf("<h2>El username puede ingresar al aplicatio</h2>");
                    }else{
                        out.printf("<h2>El username no se encuentra registrado</h2>");
                        response.encodeRedirectURL("token.html");                        
                    }
                  
                }*/  
             
            } catch (Exception ex) {
                Logger.getLogger(Servlet_Inegrator.class.getName()).log(Level.SEVERE, null, ex);
                out.printf("<h2>Error al insertar un nuevo registro</h2>");
            }
            out.println("</body>");
            out.println("</html>");
            
            
            
            
            }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
