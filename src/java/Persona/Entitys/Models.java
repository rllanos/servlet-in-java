/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persona.Entitys;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Rubio
 */
public class Models {

    public Models() {
    }
    
       
    public EntityManager getEm(){
        EntityManagerFactory factory= Persistence.createEntityManagerFactory("IntegratorPersistPU");   
        return factory.createEntityManager();
    }
    public TabPersons salvar(TabPersons persons) throws Exception{
        EntityManager em=getEm();
        try{            
            em.getTransaction().begin();
            em.persist(persons);
            em.getTransaction().commit();
        }finally{
            em.close();
        }        
        return persons;
    }
     public PersonaLogin salvar2(PersonaLogin persons) throws Exception{
        EntityManager em=getEm();
        try{            
            em.getTransaction().begin();
            em.persist(persons);
            em.getTransaction().commit();
        }finally{
            em.close();
        }        
        return persons;
    }
    public Boolean consultarPorUserName(String username){
        PersonaLogin per = null;
        EntityManager em=getEm();
        Boolean resp= true;
        try{
            per=em.find(PersonaLogin.class, username);
            if(per==null){
                resp= false;
            }
        }finally{
            em.close();
        }
        return resp;
    }
    public int consultLastId(){
        EntityManager em=getEm(); 
        int resp= 1;
        
        //Query result=em.createNativeQuery("SELECT id_Persona FROM 'personas' ORDER BY id_Persona DESC LIMIT 1");
        //String resp=result.getSingleResult().toString();                       
        return resp;
    }
}
