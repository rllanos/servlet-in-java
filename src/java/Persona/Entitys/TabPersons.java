/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persona.Entitys;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rubio
 */
@Entity
@Table(name = "tab_persons", catalog = "bd_personas", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TabPersons.findAll", query = "SELECT t FROM TabPersons t"),
    @NamedQuery(name = "TabPersons.findByIdPerso", query = "SELECT t FROM TabPersons t WHERE t.idPerso = :idPerso"),
    @NamedQuery(name = "TabPersons.findByIdentificacion", query = "SELECT t FROM TabPersons t WHERE t.identificacion = :identificacion"),
    @NamedQuery(name = "TabPersons.findByNombre", query = "SELECT t FROM TabPersons t WHERE t.nombre = :nombre")})
public class TabPersons implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_perso", nullable = false)
    private Integer idPerso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Identificacion", nullable = false, length = 50)
    private String identificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Nombre", nullable = false, length = 50)
    private String nombre;

    public TabPersons() {
    }

    public TabPersons(Integer idPerso) {
        this.idPerso = idPerso;
    }

    public TabPersons(Integer idPerso, String identificacion, String nombre) {
        this.idPerso = idPerso;
        this.identificacion = identificacion;
        this.nombre = nombre;
    }

    public Integer getIdPerso() {
        return idPerso;
    }

    public void setIdPerso(Integer idPerso) {
        this.idPerso = idPerso;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPerso != null ? idPerso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TabPersons)) {
            return false;
        }
        TabPersons other = (TabPersons) object;
        if ((this.idPerso == null && other.idPerso != null) || (this.idPerso != null && !this.idPerso.equals(other.idPerso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Persona.Entitys.TabPersons[ idPerso=" + idPerso + " ]";
    }
    
}
