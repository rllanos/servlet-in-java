/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persona.Entitys;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rubio
 */
@Entity
@Table(name = "personas", catalog = "bd_personas", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PersonaLogin.findAll", query = "SELECT p FROM PersonaLogin p"),
    @NamedQuery(name = "PersonaLogin.findByIdPersona", query = "SELECT p FROM PersonaLogin p WHERE p.idPersona = :idPersona"),
    @NamedQuery(name = "PersonaLogin.findByNombre", query = "SELECT p FROM PersonaLogin p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "PersonaLogin.findByPassword", query = "SELECT p FROM PersonaLogin p WHERE p.password = :password")})
public class PersonaLogin implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Persona", nullable = false)
    private Integer idPersona;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nombre", nullable = false, length = 30)
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "password", nullable = false, length = 50)
    private String password;

    public PersonaLogin() {
    }

    public PersonaLogin(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public PersonaLogin(Integer idPersona, String nombre, String password) {
        this.idPersona = idPersona;
        this.nombre = nombre;
        this.password = password;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPersona != null ? idPersona.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonaLogin)) {
            return false;
        }
        PersonaLogin other = (PersonaLogin) object;
        if ((this.idPersona == null && other.idPersona != null) || (this.idPersona != null && !this.idPersona.equals(other.idPersona))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Persona.Entitys.PersonaLogin[ idPersona=" + idPersona + " ]";
    }
    
}
